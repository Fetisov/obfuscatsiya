use strict;
use warnings;
open DESC, "<infile.pl";
my @somedata = <DESC>;
	
map(s/\$pi/3.14/g, @somedata);
map(s/\n//g, @somedata);
open DESC, ">infile.pl";

print DESC @somedata;